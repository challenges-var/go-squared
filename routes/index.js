const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.send([
    {
      id: 1,
      weather: 18,
      visitors: 54,
      drinksLeft: 13,
      daysLeft: 2,
      innerTemp: 22,
    }
  ])
})

module.exports = router;