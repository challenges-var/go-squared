## Backend Stack
  * concurrently
  * dotenv
  * express
  * morgan
  * node-fetch
  * nodemon

## Frontend Stack
  * React
  * Redux
  * Axios
  * Sass

### Step 1: On the client folder run npm i
### Step 2: On the root folder run npm i
### Steap 3: On the root folder run npm run dev