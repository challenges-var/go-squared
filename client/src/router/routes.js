import Home from '../views/Home';

const routes= [
  { path: '/', component: Home, exact: true },
];

export default routes;