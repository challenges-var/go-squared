import React, { Component } from 'react';
import { BrowserRouter, Route} from 'react-router-dom';
import routes from './routes';
import Header from '../components/Header';

class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Header />
          {routes.map((route, i) => <Route key={i} {...route} />)}
        </div>
      </BrowserRouter>
    );
  }
};

export default Router;