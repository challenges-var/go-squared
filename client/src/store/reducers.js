import { FETCH_DATA } from './actions';

let initState = {
  data: []
}

const rootReducer = (state = initState, action) => {
  switch(action.type) {
    case FETCH_DATA:
      return {
        ...state,
        data: action.payload,
      }
    default:
      return state
  }
}



export default rootReducer;