import axios from 'axios';

export const FETCH_DATA = 'FETCH_DATA';

const apiUrl = '/api/v1';

export const fetchData = () => {
  return (dispatch) => {
    return axios.get(apiUrl)
      .then(res => {
        dispatch({
          type: FETCH_DATA,
          payload: res.data,
        })
      })
      .catch(error => {
        throw(error);
      });
  };
};