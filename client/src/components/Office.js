import React from 'react';

const Office = ({stat, bgColor, icon}) => {
  return (
    <article className="offices__item">
      <article className={`office office--${bgColor}`}>
        <h1 className="office__title">{stat}</h1>
        <i className={`fas ${icon}`}></i>
      </article>
    </article>
  )
}

export default Office;