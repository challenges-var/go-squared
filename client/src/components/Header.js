import React from 'react';
import { Link } from 'react-router-dom';
// Assets
import Logo from '../assets/gosquared.png';

const Header = () => {
  return (
    <nav className="header">
      <Link to="/">
        <img className="header__logo" src={Logo} alt="Logo" />
      </Link>
    </nav>
  );
}

export default Header;