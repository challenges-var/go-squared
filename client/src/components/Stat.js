import React from 'react';

const Stat = ({stat, icon, bgColor}) => {
  return (
    <article className="stats__item">
      <article className={`stat stat--${bgColor}`}>
        <i className={`fas ${icon}`}></i>
        <h1 className="stat__title">{stat}</h1>
      </article>
    </article>
  )
}

export default Stat;