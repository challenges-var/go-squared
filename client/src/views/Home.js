import React, { Component } from 'react';
// Redux
import { bindActionCreators } from 'redux';
import { fetchData } from '../store/actions.js';
import { connect } from 'react-redux';
// Components
import Stat from '../components/Stat';
import Office from '../components/Office';

class Home extends Component {
  componentDidMount() {
    this.props.getData()
  }
  render() {
    const info = this.props.data;

    const dashboard = info.length ? (
      info.map(item => {
        return (
          <main key={item.id}>
            <section className="stats">
              <Stat
                stat={item.weather}
                icon="fa-temperature-high"
                bgColor="weather"
              />

              <Stat
                stat={item.visitors}
                icon="fa-users"
                bgColor="visitors"
              />

              <Stat
                stat={item.drinksLeft}
                icon="fa-beer"
                bgColor="drinks"
              />
            </section>

            <section className="offices">
              <Office
                stat={`${item.daysLeft} days left / Watering`}
                icon="fa-seedling"
                bgColor="plant"
              />

              <Office
                stat={`${item.innerTemp}° Inside the office`}
                icon="fa-building"
                bgColor="desk"
              />
            </section>
          </main>
        )
      })
    ) : (
      <h3>No Data Found!</h3>
    )

    return (
      <main className="group">
        {dashboard}
      </main>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    data: state.data
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getData: bindActionCreators(fetchData, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
