const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');

// Load Config file
dotenv.config({ path: './config.env'});

const app = express();

// Dev logging
if(process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

const port = process.env.PORT || 8000;

// Profile routes
app.use(('/api/v1'), require('./routes/index.js'));

app.listen(port, () => {
  console.log(`App running in ${process.env.NODE_ENV} mode on port ${port}!`);
});
